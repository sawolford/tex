@echo off
if [%1]==[] goto err
set scrdir=%~dp0
set TEXINPUTS=.;%scrdir%;
xelatex ./%~1
goto end
:err
echo "Usage": %0 "<filename.tex>"
:end
