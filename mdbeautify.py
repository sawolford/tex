#!/usr/bin/env python3
import argparse, fileinput, re, sys

fc = lambda prog: argparse.RawDescriptionHelpFormatter(prog, max_help_position=36, width=120)
parser = argparse.ArgumentParser(description="cleans up Markdown", formatter_class=fc)
parser.add_argument("--repl", action="append", nargs=2, metavar=("from", "to"), help='replace instances of <from> with <to> in text')
args = parser.parse_args(sys.argv[1:])
repls = args.repl if args.repl else []

dashre = re.compile("\s*-\s")
numre = re.compile("\d*\.")

NORMAL = 0
BULLET = 1
BLANK = 2
mode = NORMAL

def isbullet(line):
    if dashre.match(line): return True
    if numre.match(line): return True
    return False

for line in sys.stdin.readlines():
    line = line[:-1]
    lline = len(line)
    for old, new in repls:
        line = line.replace(old, new)
        continue
    if mode == BULLET:
        if lline == 0: mode = BLANK
        else: print(line)
        pass
    elif mode == BLANK:
        if isbullet(line): mode = BULLET
        else:
            mode = NORMAL
            print("")
            pass
        print(line)
        pass
    else:
        if isbullet(line): mode = BULLET
        print(line)
        pass
    continue
