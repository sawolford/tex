@echo off
if [%1]==[] goto err
set scrdir=%~dp0
set TEXINPUTS=%scrdir%
pandoc --standalone --number-sections --toc --include-in-header=%scrdir%pandoc.css --mathjax -f markdown_mmd %~1 -t html5+tex_math_dollars -o %~n1.html
goto end
:err
echo "Usage": %0 "<filename.tex>"
:end
