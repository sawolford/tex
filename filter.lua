-- function dump(o)
--     if type(o) == 'table' then
--        local s = '{ '
--        for k,v in pairs(o) do
--           if type(k) ~= 'number' then k = '"'..k..'"' end
--           s = s .. '['..k..'] = ' .. dump(v) .. ','
--        end
--        return s .. '} '
--     else
--        return tostring(o)
--     end
--  end

function Str(str)
    str.text = string.gsub(str.text, "_", "&lowbar;")
    return str
end

function Header(header)
    -- print(dump(header))
    if header.level == 3 and header.attr.classes[1] == "unnumbered" then
        header.level = 4
    end
    header.attr.identifier = ""
    header.attr.classes = {}
    return header
end
