@echo off
if [%1]==[] goto err
set scrdir=%~dp0
set TEXINPUTS=%scrdir%
pandoc --wrap=preserve --standalone --number-sections --atx-headers -L %scrdir%filter.lua -f latex %~1 -t markdown_mmd | python %scrdir%mdbeautify.py --repl "&lowbar;" _ > %~n1.md
goto end
:err
echo "Usage": %0 "<filename.tex>"
:end
