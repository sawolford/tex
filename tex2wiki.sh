#!/bin/sh
if [ $# -ne 1 ]; then
  echo "Usage: $0 <inputfile>"
  exit 1
fi
scrdir=$(dirname $(realpath $0))
ifile=$1
export TEXINPUTS="$scrdir:"
ofile=$(echo $ifile | cut -f1 -d.).wiki
pandoc --wrap=preserve --standalone --number-sections --atx-headers -f latex $ifile -t mediawiki > $ofile
