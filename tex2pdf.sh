#!/bin/sh
if [ $# -ne 1 ]; then
  echo "Usage: $0 <inputfile>"
  exit 1
fi
scrdir=$(dirname $(realpath $0))
ifile=$1
export TEXINPUTS="$scrdir:"
ofile=$(echo $ifile | cut -f1 -d.).pdf
tmpdir=$(mktemp -d -t genpdf-XXXXXXXX)
xelatex --output-directory=$tmpdir $ifile
rv=$?
if [ $rv -ne 0 ]; then
    echo "Errors encountered; no pdf generated"
else
    cp $tmpdir/$ofile .
    echo "Generated $ofile"
fi
rm -rf $tmpdir
