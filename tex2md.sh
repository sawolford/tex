#!/bin/sh
if [ $# -ne 1 ]; then
  echo "Usage: $0 <inputfile>"
  exit 1
fi
scrdir=$(dirname $(realpath $0))
ifile=$1
export TEXINPUTS="$scrdir:"
ofile=$(echo $ifile | cut -f1 -d.).md
pandoc --wrap=preserve --standalone --number-sections --atx-headers -L $scrdir/filter.lua -f latex $ifile -t markdown | $scrdir/mdbeautify.py --repl '&lowbar;' _ > $ofile
