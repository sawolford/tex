#!/bin/sh
if [ $# -ne 1 ]; then
  echo "Usage: $0 <inputfile>"
  exit 1
fi
scrdir=$(dirname $(realpath $0))
ifile=$1
export TEXINPUTS="$scrdir:"
ofile=$(echo $ifile | cut -f1 -d.).html
pandoc --highlight-style=$scrdir/tex.theme --standalone --number-sections --toc --include-in-header=$scrdir/pandoc.css --mathjax -f markdown -t html5+tex_math_dollars $ifile -o $ofile
