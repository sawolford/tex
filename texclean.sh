#!/bin/sh
if [ $# -ne 1 ]; then
  echo "Usage: $0 <base>"
  exit 1
fi
base=$1
rm -f $base.aux $base.out $base.toc $base.fls $base.synctex.gz $base.log $base.fdb_latexmk
